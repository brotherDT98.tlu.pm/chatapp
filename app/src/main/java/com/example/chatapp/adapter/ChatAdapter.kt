package com.example.chatapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.chatapp.R
import com.example.chatapp.model.Chat
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.item_left.view.*
import kotlinx.android.synthetic.main.item_user.view.imgUser
import kotlinx.android.synthetic.main.item_user.view.layoutUser

class ChatAdapter(private val chatList:ArrayList<Chat>, private val listener:OnChatItemClickListener, private val mContext:Context) : RecyclerView.Adapter<ChatAdapter.ChatViewHolder>() {

    private val MESSAGE_TYPE_LEFT = 0
    private val MESSAGE_TYPE_RIGHT = 1
    private var firebaseUser:FirebaseUser? = null

    inner class ChatViewHolder(view:View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        val message:TextView = view.tvMessage
        val imgUserImage:CircleImageView = view.imgUser
        private val layOutUser:LinearLayout = view.layoutUser

        init {
            layOutUser.setOnClickListener(this)
        }
        override fun onClick(p0: View?) {
           val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                listener.onChatClicked(position)
            }
        }

    }

    interface OnChatItemClickListener {
        fun onChatClicked(position:Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatViewHolder {
        return if (viewType == MESSAGE_TYPE_RIGHT) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_right,parent,false)
            ChatViewHolder(view)
        }else {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_left,parent,false)
            ChatViewHolder(view)
        }
    }

    override fun getItemCount(): Int {
        return chatList.size
    }

    override fun onBindViewHolder(holder: ChatViewHolder, position: Int) {
        val currentChat = chatList[position]
        holder.message.text = currentChat.message
    }

    override fun getItemViewType(position: Int): Int {
       firebaseUser = FirebaseAuth.getInstance().currentUser
        return if (chatList[position].senderId == firebaseUser!!.uid) {
            MESSAGE_TYPE_RIGHT
        } else {
            MESSAGE_TYPE_LEFT
        }
    }

}