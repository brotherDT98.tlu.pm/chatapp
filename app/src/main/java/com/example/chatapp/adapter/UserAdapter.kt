package com.example.chatapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.chatapp.R
import com.example.chatapp.model.User
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.item_user.view.*

class UserAdapter(private val userList:ArrayList<User>, private val listener:OnUserItemClickListener, private val mContext:Context) : RecyclerView.Adapter<UserAdapter.UserViewHolder>() {

    inner class UserViewHolder(view:View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        val txtUserName:TextView = view.tvUserName
        val imgUserImage:CircleImageView = view.imgUser
        val txtTemp:TextView = view.tvTemp
        private val layOutUser:LinearLayout = view.layoutUser

        init {
            layOutUser.setOnClickListener(this)
        }
        override fun onClick(p0: View?) {
           val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                listener.onUserClicked(position)
            }
        }

    }

    interface OnUserItemClickListener {
        fun onUserClicked(position:Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_user,parent,false)
        return UserViewHolder(view)
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
       val currentUser = userList[position]
        holder.txtUserName.text = currentUser.userName
        if (currentUser.profileImage == "") {
            holder.imgUserImage.setImageResource(R.drawable.ic_user)
        } else {
            Glide.with(mContext).load(currentUser.profileImage)
                .placeholder(R.drawable.ic_user).into(holder.imgUserImage)
        }
    }

}