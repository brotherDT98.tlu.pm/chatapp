package com.example.chatapp.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.chatapp.R
import com.example.chatapp.model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_profile.*
import java.io.IOException
import java.util.*
import kotlin.collections.HashMap

class ProfileActivity : AppCompatActivity() {

    private var firebaseUser: FirebaseUser? = null
    private var databaseReference: DatabaseReference? = null
    private var filePath: Uri? = null
    private val PICK_IMAGE_REQUEST: Int = 1998
    private var storage: FirebaseStorage? = null
    private var storageRef: StorageReference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        firebaseUser = FirebaseAuth.getInstance().currentUser
        databaseReference =
            FirebaseDatabase.getInstance().getReference("Users").child(firebaseUser!!.uid)

        storage = FirebaseStorage.getInstance()
        storageRef = storage?.reference

        databaseReference?.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(this@ProfileActivity, error.message, Toast.LENGTH_SHORT).show()
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val user = snapshot.getValue(User::class.java)
                etUserName.setText(user?.userName)
                if (user?.profileImage == "") {
                    imgUser.setImageResource(R.drawable.ic_user)
                } else {
                    Glide.with(this@ProfileActivity).load(user?.profileImage)
                        .placeholder(R.drawable.ic_user).into(imgUser)
                }
            }

        })

        imgBack.setOnClickListener {
            onBackPressed()
        }

        imgUser.setOnClickListener {
            chooseImage()
        }

        btnSave.setOnClickListener {
            uploadImage()
            progressBar.visibility = View.VISIBLE
        }

        imgLogOut.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }

    private fun chooseImage() {
        intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_OPEN_DOCUMENT
        startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST && resultCode != null) {
            filePath = data?.data
            try {
                var bitmap = MediaStore.Images.Media.getBitmap(contentResolver, filePath)
                imgUser.setImageBitmap(bitmap)
                btnSave.visibility = View.VISIBLE
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun uploadImage() {
        if (filePath != null) {
            var ref: StorageReference = storageRef!!.child("image/" + UUID.randomUUID().toString())
            ref.putFile(filePath!!).addOnSuccessListener {
                val hashMap:HashMap<String,String> = HashMap()
                hashMap["userName"] = etUserName.text.toString()
                hashMap["profileImage"] = filePath.toString()
                databaseReference?.updateChildren(hashMap as Map<String, Any>)
                progressBar.visibility = View.GONE
                Toast.makeText(this@ProfileActivity, "Uploaded", Toast.LENGTH_SHORT).show()
                btnSave.visibility = View.GONE
            }
                .addOnFailureListener {
                    progressBar.visibility = View.GONE
                    Toast.makeText(this@ProfileActivity, it.message, Toast.LENGTH_SHORT).show()
                }
                .addOnProgressListener {
                    var progress: Double = (100.0 * it.bytesTransferred / it.totalByteCount)
                }
        }
    }
}