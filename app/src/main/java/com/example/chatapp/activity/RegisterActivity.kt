package com.example.chatapp.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import com.example.chatapp.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    private var mAuth:FirebaseAuth? = null
    private var databaseReference:DatabaseReference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        mAuth = FirebaseAuth.getInstance()

        btnSignUp.setOnClickListener{

            val userName = etName.text.toString()
            val email = etEmail.text.toString()
            val pwd = etPwd.text.toString()
            val confirmPwd = etRePwd.text.toString()

            if (TextUtils.isEmpty(userName)) {
                Toast.makeText(this, "User name is required !", Toast.LENGTH_SHORT).show()
            }
            else if (TextUtils.isEmpty(email)) {
                Toast.makeText(this, "Email is required !", Toast.LENGTH_SHORT).show()
            }
            else if (TextUtils.isEmpty(pwd)) {
                Toast.makeText(this, "Password is required !", Toast.LENGTH_SHORT).show()
            }
            else if (TextUtils.isEmpty(confirmPwd)) {
                Toast.makeText(this, "Confirm password is required !", Toast.LENGTH_SHORT).show()
            }
            else if (pwd != confirmPwd){
                Toast.makeText(this, "Password not match !", Toast.LENGTH_SHORT).show()
            }
            else registerUser(userName,email,pwd)
        }

        btnSignIn.setOnClickListener {
            intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
    }

    private fun registerUser(userName:String,email:String, pwd: String) {
        mAuth?.createUserWithEmailAndPassword(email,pwd)?.addOnCompleteListener { it ->
            if (it.isSuccessful) {
                val user: FirebaseUser? = mAuth?.currentUser
                val userId: String = user!!.uid

                databaseReference = FirebaseDatabase.getInstance().getReference("Users").child(userId)

                val hashMap:HashMap<String,String> = HashMap()
                hashMap["userId"] = userId
                hashMap["userName"] = userName
                hashMap["profileImage"] = ""

                databaseReference!!.setValue(hashMap).addOnCompleteListener {task ->
                    when {
                        task.isSuccessful -> {
                            etEmail.setText("")
                            etName.setText("")
                            etPwd.setText("")
                            etRePwd.setText("")
                            Toast.makeText(this, "Register success !", Toast.LENGTH_SHORT).show()
                        }
                        else -> {
                            Toast.makeText(this, "Something error !",Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            } else {
                Toast.makeText(this, "User is exist with email !",Toast.LENGTH_SHORT).show()
            }
        }
    }
}