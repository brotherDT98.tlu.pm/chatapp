package com.example.chatapp.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.chatapp.R
import com.example.chatapp.adapter.ChatAdapter
import com.example.chatapp.model.Chat
import com.example.chatapp.model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_chat.*

class ChatActivity : AppCompatActivity(), ChatAdapter.OnChatItemClickListener {

    private var firebaseUser: FirebaseUser? = null
    private var databaseReference: DatabaseReference? = null
    private var chatList = ArrayList<Chat>()
    private var chatAdapter: ChatAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        recycler_view_chat.layoutManager = LinearLayoutManager(this)
        recycler_view_chat.setHasFixedSize(true)


        val userId = intent.getStringExtra("userId")

        firebaseUser = FirebaseAuth.getInstance().currentUser
        databaseReference = FirebaseDatabase.getInstance().getReference("Users").child(userId!!)

        databaseReference?.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {

            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val user = snapshot.getValue(User::class.java)
                tvUserName.text = user!!.userName
                if (user.profileImage == "") {
                    imgProfile.setImageResource(R.drawable.ic_user)
                } else {
                    Glide.with(this@ChatActivity).load(user.profileImage)
                        .placeholder(R.drawable.ic_user).into(imgProfile)
                }
            }
        })

        imgBack.setOnClickListener {
            onBackPressed()
        }

        btnSend.setOnClickListener {
            var message: String = etMessage.text.toString()
            if (message.isEmpty()) {
                Toast.makeText(this, "Message is require !", Toast.LENGTH_SHORT).show()
                etMessage.setText("")
            } else {
                sendMessage(firebaseUser!!.uid, userId, message)
                etMessage.setText("")
            }
        }

        readMessage(firebaseUser!!.uid, userId)
    }

    private fun sendMessage(senderId: String, receiverId: String, message: String) {
        databaseReference = FirebaseDatabase.getInstance().reference

        val hashMap: HashMap<String, String> = HashMap()
        hashMap["senderId"] = senderId
        hashMap["receiverId"] = receiverId
        hashMap["message"] = message

        databaseReference?.child("Chat")?.push()?.setValue(hashMap)
    }

    fun readMessage(senderId: String, receiverId: String) {
        databaseReference = FirebaseDatabase.getInstance().getReference("Chat")

        databaseReference?.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {

            }

            override fun onDataChange(snapshot: DataSnapshot) {
                chatList.clear()
                for (data in snapshot.children) {
                    val chat = data.getValue(Chat::class.java)
                    if ((chat!!.senderId == senderId && chat!!.receiverId == receiverId) || (chat!!.senderId == receiverId && chat!!.receiverId == senderId)) {
                        chatList.add(chat)
                    }
                }
                chatAdapter = ChatAdapter(chatList, this@ChatActivity, this@ChatActivity)
                recycler_view_chat.adapter = chatAdapter
            }

        })
    }

    override fun onChatClicked(position: Int) {

    }
}