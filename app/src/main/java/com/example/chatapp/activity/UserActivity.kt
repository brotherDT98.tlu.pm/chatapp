package com.example.chatapp.activity

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.chatapp.R
import com.example.chatapp.adapter.UserAdapter
import com.example.chatapp.model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_user.*
import kotlinx.android.synthetic.main.activity_user.imgBack
import kotlinx.android.synthetic.main.activity_user.imgProfile

class UserActivity : AppCompatActivity(), UserAdapter.OnUserItemClickListener {

    private var userList = ArrayList<User>()
    private var userAdapter: UserAdapter? = null
    private var firebaseUser: FirebaseUser? = null
    private var databaseReference: DatabaseReference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)

        firebaseUser = FirebaseAuth.getInstance().currentUser
        databaseReference =
            FirebaseDatabase.getInstance().getReference("Users").child(firebaseUser!!.uid)
        databaseReference?.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(this@UserActivity, error.message, Toast.LENGTH_SHORT).show()
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val user = snapshot.getValue(User::class.java)
                Glide.with(this@UserActivity).load(user?.profileImage)
                    .placeholder(R.drawable.ic_user).into(imgProfile)
            }
        })

        imgBack.setOnClickListener {
            onBackPressed()
        }
        imgProfile.setOnClickListener {
            startActivity(Intent(this, ProfileActivity::class.java))
        }
        getUsers()
        recycler_view_user.layoutManager = LinearLayoutManager(this)
        recycler_view_user.setHasFixedSize(true)
        userAdapter = UserAdapter(userList, this, this)
        recycler_view_user.adapter = userAdapter
    }

    private fun getUsers() {
        databaseReference =
            FirebaseDatabase.getInstance().getReference("Users")
        databaseReference?.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(this@UserActivity, error.message, Toast.LENGTH_SHORT).show()
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                userList.clear()
                for (data in snapshot.children) {
                    val user = data.getValue(User::class.java)
                    if (user!!.userId != firebaseUser?.uid) {
                        userList.add(user)
                    }
                }
                userAdapter?.notifyDataSetChanged()
            }

        })
    }

    override fun onUserClicked(position: Int) {
        intent = Intent(this, ChatActivity::class.java)
        intent.putExtra("userId", userList[position].userId)
        startActivity(intent)
    }

}