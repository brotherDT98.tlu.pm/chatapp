package com.example.chatapp.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import com.example.chatapp.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private var mAuth:FirebaseAuth? = null
    private var firebaseUser:FirebaseUser? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        mAuth = FirebaseAuth.getInstance()
        firebaseUser = mAuth!!.currentUser

        if (firebaseUser != null) {
            intent = Intent(this, UserActivity::class.java)
            startActivity(intent)
        }

        btnSignIn.setOnClickListener {
            val email = etEmail.text.toString()
            val pwd = etPwd.text.toString()

            when {
                TextUtils.isEmpty(email) -> {
                    Toast.makeText(this, "Email is required !", Toast.LENGTH_SHORT).show()
                }
                TextUtils.isEmpty(pwd) -> {
                    Toast.makeText(this, "Password is required !", Toast.LENGTH_SHORT).show()
                }
                else -> mAuth?.signInWithEmailAndPassword(email,pwd)?.addOnCompleteListener {
                    if (it.isSuccessful) {
                        etEmail.setText("")
                        etPwd.setText("")
                        intent = Intent(this, UserActivity::class.java)
                        startActivity(intent)
                    }else {
                        Toast.makeText(this, "Something error !", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }

        btnSignUp.setOnClickListener{
            intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }
    }
}